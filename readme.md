### Bangodiff
Programme développé dans le cadre du PS5. 

Outil d'analyse de résultats pour le programme d'analyse ADN Bangolin, développé par le groupe Wegmann de l'Université de Fribourg.

#### Liens

[Forge du projet PS5](https://redmine.forge.hefr.ch/projects/i3-ps5-bangolin)

[Repo git du projet PS5](https://gitlab.forge.hefr.ch/julien.harle/ps5-bangolin)

[Repo bitbucket de Bangolin](https://bitbucket.org/wegmannlab/bangolin)