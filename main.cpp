#include <iostream>
#include <fstream>
#include <cfloat>
#include <cmath>
#include <set>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <iomanip>


int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: bangodiff <file1> <file2> [output_file_col_analysis] [output_file_line_analysis]" << std::endl;
        return 1;
    }

    std::ifstream file1, file2;
    file1.open(argv[1]);
    file2.open(argv[2]);

    if (!file1.is_open() || !file2.is_open()) {
        std::cerr << "Error opening files" << std::endl;
        return 1;
    }
    std::printf("Comparing files %s and %s ...\n", argv[1], argv[2]);

    //get column names
    std::string namesLine1, namesLine2;
    std::vector<std::string> colNames1, colNames2;

    std::getline(file1, namesLine1);
    std::getline(file2, namesLine2);

    boost::split(colNames1, namesLine1, [](char c){return c == '\t';});
    boost::split(colNames2, namesLine2, [](char c){return c == '\t';});

    if(colNames1 != colNames2) {
        std::cerr << "The files have differing columns !" << std::endl;
        return 1;
    }

    //values indexed first by lines then cols
    std::vector<std::vector<double>> values1;
    std::vector<std::vector<double>> values2;
    std::vector<std::string> firstCol1, firstCol2; //first column of each file is a string to identify the line
    values1.resize(colNames1.size());
    values2.resize(colNames2.size());

    std::string currLine1, currLine2;

    //get all the values in the 2D values vector
    while(std::getline(file1, currLine1) && std::getline(file2, currLine2)) {
        //split values into string vectors
        std::vector<std::string> splitValues1, splitValues2;
        boost::split(splitValues1, currLine1, [](char c){return c == '\t';} );
        boost::split(splitValues2, currLine2, [](char c){return c == '\t';} );

        firstCol1.push_back(splitValues1[0]);
        firstCol2.push_back(splitValues2[0]);

        //cast strings into doubles
        std::vector<double> tempValues1(splitValues1.size()), tempValues2(splitValues2.size());
        std::transform(splitValues1.begin() + 1, splitValues1.end(), tempValues1.begin(), [](const std::string& val)
        {
            return std::stod(val);
        });

        std::transform(splitValues2.begin() + 1, splitValues2.end(), tempValues2.begin(), [](const std::string& val)
        {
            return std::stod(val);
        });

        for(int col = 0; col < colNames1.size(); col++) {
            values1[col].push_back(tempValues1[col]);
            values2[col].push_back(tempValues2[col]);
        }
    }

    std::ofstream outCol;
    //if an output file was specified, use that
    if(argc >= 4) {
        outCol.open(argv[3]);
    }
    //if not, just put res.txt in current folder
    else {
        outCol.open("by_col_res.txt");
    }
    outCol << "file1 : " << argv[1] << std::endl;
    outCol << "file2 : " << argv[2] << std::endl;
    outCol << "\nColumn_name\tFile\tMax\t\tMin\t\tAvg" << std::endl;

    //line by line analysis
    std::ofstream outLine;
    if(argc == 5) {
        outLine.open(argv[4]);
    }
        //if not, just put res.txt in current folder
    else {
        outLine.open("by_line_res.txt");
    }
    outLine << "file1 : " << argv[1] << std::endl;
    outLine << "file2 : " << argv[2] << std::endl;
    outLine << "\nColumn_name\tNb_diff\tAvg_abs_err\tMax_abs_err\tAvg_rel_err\tMax_rel_err\tMedian_f1/Median_f2" << std::endl;


    int nbCols = values1.size() - 1; //minus 1 because the last col is just empty (boost split ?)
    int nbLines = values1[0].size();
    std::vector<double> colDiffSums(nbCols);


    for(int col = 0; col < nbCols; col++) {

        double file1Max = 0, file1Min = DBL_MAX, file1Avg = 0, file2Max = 0, file2Min = DBL_MAX, file2Avg = 0;
        double currColAbsErrSum = 0;
        double currColRelErrSum = 0;
        double maxRelErr = 0;
        double maxAbsErr = 0;
        int nbDiff = 0;
        std::vector<double> currColVals1(nbLines);
        std::vector<double> currColVals2(nbLines);

        for(int line = 0; line < nbLines; line++) {
            double val1 = values1[col][line];
            double val2 = values2[col][line];
            if(val1 != val2) nbDiff++;
            currColVals1[line] = val1;
            currColVals2[line] = val2;

            //for /by column" analysis
            file1Avg += val1;
            file2Avg += val2;

            if(val1 < file1Min)
                file1Min = val1;
            else if(val1 > file1Max)
                file1Max = val1;

            if(val2 < file2Min)
                file2Min = val2;
            else if(val2 > file2Max)
                file2Max = val2;

            //for "by line" analysis
            if(val1 == val2)
                continue;
            if(val1 == 0 || val2 == 0) {
                val1 += 1;
                val2 += 1;
            }

            double currColAbsErr = std::abs(val1 - val2);
            currColAbsErrSum += currColAbsErr;
            if(currColAbsErr > maxAbsErr) {
                maxAbsErr = currColAbsErr;
            }

            double relErr = currColAbsErr / val2 * 100;
            currColRelErrSum += relErr;
            if(relErr > maxRelErr) {
                maxRelErr = relErr;
            }
        }

        file1Avg /= nbLines;
        file2Avg /= nbLines;

        double maxDiff = std::abs(file1Max-file2Max)*100;
        maxDiff = file1Max < file2Max ? maxDiff / file2Max : maxDiff / file1Max;

        double minDiff = std::abs(file1Min-file2Min)*100;
        minDiff = file1Min < file2Min ? minDiff / file2Min : minDiff / file1Min;

        double avgDiff = std::abs(file1Avg-file2Avg)*100;
        avgDiff = file1Avg < file2Avg ? avgDiff / file2Avg : avgDiff / file1Avg;

        outCol << colNames1[col + 1] << std::endl;
        outCol << "            file1:";
        outCol << "\t" << std::setprecision(4) << file1Max;
        outCol << "\t" << std::setprecision(4) << file1Min;
        outCol << "\t" << std::setprecision(4) << file1Avg << std::endl;

        outCol << "            file2:";
        outCol << "\t" << std::setprecision(4) << file2Max;
        outCol << "\t" << std::setprecision(4) << file2Min;
        outCol << "\t" << std::setprecision(4) << file2Avg << std::endl;

        outCol << "            diff:";
        outCol << "\t" << std::setprecision(4) << std::abs(maxDiff) << "%";
        outCol << "\t" << std::setprecision(4) << std::abs(minDiff) << "%";
        outCol << "\t" << std::setprecision(4) << std::abs(avgDiff) << "%" << std::endl;



        double currColAvgAbsErr = std::abs(currColAbsErrSum / nbLines);
        double currColAvgRelErr = currColRelErrSum / nbLines;

        //find the median value
        std::sort(currColVals1.begin(), currColVals1.end());
        std::sort(currColVals2.begin(), currColVals2.end());
        double median1 = currColVals1[currColVals1.size()/2];
        double median2 = currColVals2[currColVals2.size()/2];

        //nb val differentes, ecart-type

        outLine << colNames1[col + 1] << "\t" << nbDiff << "\t" << currColAvgAbsErr << "\t" << maxAbsErr << "\t" << currColAvgRelErr
        << "%" << "\t" << maxRelErr << "%" << "\t" << median1 << "/" << median2 << std::endl;
    }

    outCol.close();
    outLine.close();
    return 0;
}
